{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Server
  ( startServer
  , getElmDefinitions
  ) where

-- Imports

import GHC.Generics
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Data.List (delete)
import Data.Maybe
import Data.Aeson (ToJSON, FromJSON)
import qualified Data.Aeson as JSON
import qualified Network.WebSockets as WS
import Control.Monad (forever)
import Control.Concurrent (MVar, newMVar, modifyMVar_, readMVar)
import Control.Exception (catch, throw)
import qualified Control.Logging as Log
import qualified Web.Hashids as HI
import Game (Game)
import qualified Game
import qualified Generics.SOP as SOP
import Language.Haskell.To.Elm
import Language.Elm.Definition (Definition)

-- Types

data ServerState = ServerState
  { serverGames :: MVar (Map.Map Game.GameID (MVar Game))
  , serverClients :: MVar (Map.Map Game.PlayerID Client)
  , serverIdleClients :: MVar [Game.PlayerID]
  , serverSalt :: Text
  }

data Client = Client
  { clientConnection :: WS.Connection
  , clientGame :: MVar (Maybe Game.GameID)
  , clientID :: Game.PlayerID
  }

data ClientMessage
  = ClientCreateGame Text Text
  | ClientJoinGame Game.GameID Text
  | ClientLeaveGame
  | ClientStartGame
  | ClientResetGame
  | ClientPickCards Int Int Int
  | ClientRequestCards
  | ClientEndGame
  | ClientChangeSettings Game.Settings
  deriving (Generic, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType ClientMessage where
  elmDefinition = Just $ deriveElmTypeDefinition @ClientMessage defaultOptions "Api.Messages.ClientMessage"

instance HasElmEncoder JSON.Value ClientMessage where
  elmEncoderDefinition =
    Just $ deriveElmJSONEncoder @ClientMessage defaultOptions JSON.defaultOptions "Api.Messages.clientMessageEncoder"

data ServerMessage
  = ServerGameUpdate Game
  | ServerGameListUpdate [Game.GameDesc]
  | ServerError Text
  | ServerProvideID Game.PlayerID
  deriving (Generic, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType ServerMessage where
  elmDefinition = Just $ deriveElmTypeDefinition @ServerMessage defaultOptions "Api.Messages.ServerMessage"

instance HasElmDecoder JSON.Value ServerMessage where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @ServerMessage defaultOptions JSON.defaultOptions "Api.Messages.serverMessageDecoder"

-- Websocket Server

startServer :: Text -> Int -> Text -> Text -> IO ()
startServer address port path salt = do
  mGames <- newMVar Map.empty
  mClients <- newMVar Map.empty
  mIdleClients <- newMVar []
  let state = ServerState
                { serverGames = mGames
                , serverClients = mClients
                , serverIdleClients = mIdleClients
                , serverSalt = salt
                }
  WS.runServer (T.unpack address) port $ serverApplication path state

serverApplication :: Text -> ServerState -> WS.ServerApp
serverApplication path state pending = do
  -- Test if connection request is valid
  let request = WS.pendingRequest pending
  if path == (TE.decodeUtf8 $ WS.requestPath request)
    then do
      conn <- WS.acceptRequest pending
      Log.log "Received new connection on correct path"
      clients <- readMVar $ serverClients state
      let pid = newPlayerID (serverSalt state) $ Map.keys clients
      mGid <- newMVar Nothing
      let newClient = Client { clientConnection = conn, clientGame = mGid, clientID = pid }
      modifyMVar_ (serverClients state) $ \cs -> return $ Map.insert pid newClient cs
      modifyMVar_ (serverIdleClients state) $ \ids -> return (pid : ids)
      WS.sendTextData conn $ JSON.encode $ ServerProvideID pid
      sendGameList state newClient
      WS.withPingThread conn 30 (return ())
        $ catch (forever $ handleClientCommands state newClient) $ \e -> case (e :: WS.ConnectionException) of
          WS.CloseRequest _ _ -> clientDisconnected state newClient
          WS.ConnectionClosed -> clientDisconnected state newClient
          _ -> throw e
    else do
      Log.log "Rejecting request due to bad route"
      WS.rejectRequestWith pending rejectBadRoute

handleClientCommands :: ServerState -> Client -> IO ()
handleClientCommands state client = do
  msg <- WS.receiveDataMessage $ clientConnection client
  case msg of
    WS.Binary _ -> do
      Log.warn "Ignoring binary message"
      sendError client "Binary messages are ignored"
    WS.Text byteString _ -> case JSON.decode byteString :: Maybe ClientMessage of
      Nothing -> do
        Log.warn "Received invalid message from player"
        sendError client "You sent an invalid message. Ignoring it."
      Just cmd -> do
        case cmd of
          ClientCreateGame ownerName gameName->
            let owner = Game.newPlayer ownerName $ clientID client
            in clientWhenIdle state client "You can't create a game if you have already joined one" 
              $ clientCreateGame owner gameName
          ClientJoinGame gid name ->
            let player = Game.newPlayer name $ clientID client
            in clientWhenIdle state client "You can't join a game if you have already joined one"
              $ clientJoinGame gid player
          ClientLeaveGame -> clientForGame state client "You can't leave a game if you haven't joined one"
            $ clientLeaveGame False
          ClientStartGame -> clientForGame state client "You can't start a game if you haven't joined one"
            clientStartGame
          ClientResetGame -> clientForGame state client "You can't reset a game if you haven't joined one"
            clientResetGame
          ClientPickCards p1 p2 p3 -> clientForGame state client "You can't pick cards if you haven't joined a game"
            $ clientPickCards (p1, p2, p3)
          ClientRequestCards -> clientForGame state client "You can't request cards if you haven't joined a game"
            clientRequestCards
          ClientEndGame -> clientForGame state client "You cant't end a game if you haven't joined one"
            clientEndGame
          ClientChangeSettings settings -> clientForGame state client
            "You can't change the game settings if you haven't joined a game" $ clientChangeSettings settings

clientForGame :: ServerState -> Client -> Text -> (ServerState -> Client -> MVar Game -> IO ()) -> IO ()
clientForGame state client errorMsg func = do
  mGid <- readMVar $ clientGame client
  case mGid of
    Nothing -> sendError client errorMsg
    Just gid -> do
      games <- readMVar $ serverGames state
      case Map.lookup gid games of
        Nothing -> Log.log $ T.concat
          [ "Game "
          , logShow gid
          , " specified for client "
          , logShow $ clientID client
          , " not found"
          ]
        Just mGame -> func state client mGame

clientWhenIdle :: ServerState -> Client -> Text -> (ServerState -> Client -> IO ()) -> IO ()
clientWhenIdle state client errorMsg func = do
  mGid <- readMVar $ clientGame client
  case mGid of
    Just _ -> sendError client errorMsg
    Nothing -> func state client

clientDisconnected :: ServerState -> Client -> IO ()
clientDisconnected state client = do
  Log.log "Client disconnected"
  mGid <- readMVar $ clientGame client
  case mGid of
    Nothing -> return ()
    Just gid -> do
      games <- readMVar $ serverGames state
      case Map.lookup gid games of
        Nothing -> Log.warn "Could not find specified game"
        Just mGame -> clientLeaveGame True state client mGame
  modifyMVar_ (serverClients state) $ \cs -> return $ Map.delete (clientID client) cs
  modifyMVar_ (serverIdleClients state) $ \ids -> return $ delete (clientID client) ids

-- Individual Actions

clientCreateGame :: Game.Player -> Text -> ServerState -> Client -> IO ()
clientCreateGame owner name state client = do
  games <- readMVar $ serverGames state
  let gid = newGameID (serverSalt state) $ Map.keys games
  mGame <- Game.newGame gid owner name >>= newMVar
  modifyMVar_ (serverGames state) $ \gs -> return $ Map.insert gid mGame gs
  modifyMVar_ (clientGame client) $ \_ -> return $ Just gid
  modifyMVar_ (serverIdleClients state) $ \ids -> return $ delete (clientID client) ids
  Log.log $ T.concat ["Player ", logShow $ clientID client, " created a new game with ID ", logShow gid]
  sendGameUpdate state client
  updateIdleClients state

clientJoinGame :: Game.GameID -> Game.Player -> ServerState -> Client -> IO ()
clientJoinGame gid player state client = do
  games <- readMVar $ serverGames state
  case Map.lookup gid games of
    Nothing -> do
      Log.warn "Client tried to join non existent game"
      sendError client "The game you tried to join does not exist"
    Just mGame -> do
      modifyMVar_ (clientGame client) $ \_ -> return $ Just gid
      modifyMVar_ (serverIdleClients state) $ \ids -> return $ delete (clientID client) ids
      modifyMVar_ mGame $ \g -> return $ Game.addPlayer player g
      game <- readMVar mGame
      updateAllPlayers state game
      updateIdleClients state

clientLeaveGame :: Bool -> ServerState -> Client -> MVar Game -> IO ()
clientLeaveGame disc state client mGame = do
  modifyMVar_ mGame $ \g -> return $ Game.removePlayer (clientID client) g
  game <- readMVar mGame
  case Game.isOrphaned game of
    True -> modifyMVar_ (serverGames state) $ \gs -> return $ Map.delete (Game.getGameID game) gs
    False -> updateAllPlayers state game
  if not disc then
    modifyMVar_ (serverIdleClients state) $ \ids -> return (clientID client : ids)
    else return ()
  updateIdleClients state

clientStartGame :: ServerState -> Client -> MVar Game -> IO ()
clientStartGame state client mGame = do
  game <- readMVar mGame
  if (clientID client) == (Game.getPlayerID $ Game.getOwner game)
    then do
      modifyMVar_ mGame Game.startGame
      Log.log $ T.concat ["Player ", logShow $ clientID client, " started the game ", logShow $ Game.getGameID game]
      updateAllPlayers state game
      updateIdleClients state
    else
      sendError client "You can only start the game if you are the owner"

clientPickCards :: (Int, Int, Int) -> ServerState -> Client -> MVar Game -> IO ()
clientPickCards pick state client mGame = do
  modifyMVar_ mGame $ \g -> return $ Game.pickCards pick (clientID client) g
  game <- readMVar mGame
  updateAllPlayers state game
  case Game.isEnded game of
    True -> updateIdleClients state
    False -> return ()

clientResetGame :: ServerState -> Client -> MVar Game -> IO ()
clientResetGame state client mGame = do
  game <- readMVar mGame
  if (clientID client) == (Game.getPlayerID $ Game.getOwner game)
    then do
      modifyMVar_ mGame $ \g -> return $ Game.resetGame g
      updateAllPlayers state game
      updateIdleClients state
    else
      sendError client "You can only reset the game if you are the owner"

clientRequestCards :: ServerState -> Client -> MVar Game -> IO ()
clientRequestCards state client mGame = do
  modifyMVar_ mGame $ \g -> return $ Game.requestCards (clientID client) g
  game <- readMVar mGame
  updateAllPlayers state game

clientEndGame :: ServerState -> Client -> MVar Game -> IO ()
clientEndGame state client mGame = do
  game <- readMVar mGame
  if (clientID client) == (Game.getPlayerID $ Game.getOwner game)
    then do
      modifyMVar_ mGame $ \g -> return $ Game.endGame g
      updateAllPlayers state game
      updateIdleClients state
    else
      sendError client "You can't end the game unless you are the owner"

clientChangeSettings :: Game.Settings -> ServerState -> Client -> MVar Game -> IO ()
clientChangeSettings settings state client mGame = do
  game <- readMVar mGame
  if (clientID client) == (Game.getPlayerID $ Game.getOwner game)
    then do
      modifyMVar_ mGame $ \g -> return $ Game.changeSettings settings g
      updateAllPlayers state game
    else
      sendError client "Yo can only change the game settings if you are the owner"

-- Update Functions

updateAllPlayers :: ServerState -> Game -> IO ()
updateAllPlayers state game = do
  let ids = map Game.getPlayerID $ Game.getAllPlayers game
  allClients <- readMVar $ serverClients state
  let clients = map (\k -> Map.lookup k allClients) ids
  flip mapM_ clients (\mc -> case mc of
    Nothing -> return ()
    Just c -> sendGameUpdate state c)

updateIdleClients :: ServerState -> IO ()
updateIdleClients state = do
  clients <- readMVar $ serverClients state
  idle <- readMVar $ serverIdleClients state
  flip mapM_ idle $ \cid -> case Map.lookup cid clients of
    Nothing -> Log.warn "Could not find specified client"
    Just client -> sendGameList state client

sendGameList :: ServerState -> Client -> IO ()
sendGameList state client = do
  mGames <- readMVar $ serverGames state
  games <- mapM readMVar $ Map.elems mGames
  WS.sendTextData (clientConnection client) $ JSON.encode $ ServerGameListUpdate $ map Game.getGameDesc games

sendGameUpdate :: ServerState -> Client -> IO ()
sendGameUpdate state client = do
  mGid <- readMVar $ clientGame client
  case mGid of
    Nothing -> return ()
    Just gid -> do
      games <- readMVar $ serverGames state
      case Map.lookup gid games of
        Nothing -> Log.warn
          $ T.concat ["Game ", logShow gid, " specified for client ", logShow $ clientID client, " not found"]
        Just mGame -> do
          game <- readMVar mGame
          WS.sendTextData (clientConnection client) $ JSON.encode $ ServerGameUpdate game

sendError :: Client -> Text -> IO ()
sendError client msg =
  WS.sendTextData (clientConnection client) $ JSON.encode $ ServerError msg

-- Reject Request Messages

rejectBadRoute :: WS.RejectRequest
rejectBadRoute = WS.RejectRequest
  { WS.rejectCode = 404
  , WS.rejectMessage = TE.encodeUtf8 "Not found"
  , WS.rejectHeaders = []
  , WS.rejectBody = TE.encodeUtf8 "Bad route"
  }

-- Unique IDs

newID :: Text -> [Text] -> Text
newID salt [] =
  let hashContext = HI.hashidsMinimum (TE.encodeUtf8 salt) 15
  in TE.decodeUtf8 $ HI.encode hashContext 1
newID salt ids =
  let hashContext = HI.hashidsMinimum (TE.encodeUtf8 salt) 15
  in TE.decodeUtf8 $ HI.encode hashContext $ 1 + (maximum $ map (head . HI.decode hashContext . TE.encodeUtf8) ids)

newGameID :: Text -> [Game.GameID] -> Game.GameID
newGameID salt ids =
  let modSalt = T.append salt "GameID"
  in Game.GameID $ newID modSalt $ map (\g -> case g of Game.GameID gid -> gid) ids

newPlayerID :: Text -> [Game.PlayerID] -> Game.PlayerID
newPlayerID salt ids =
  let modSalt = T.append salt "PlayerID"
  in Game.PlayerID $ newID modSalt $ map (\p -> case p of Game.PlayerID pid -> pid) ids

-- Log helpers

logShow :: Show a => a -> Text
logShow dat =
  T.pack $ show dat

-- Generate Elm Code

getElmDefinitions :: [Definition]
getElmDefinitions =
  catMaybes
    [ elmDefinition @ServerMessage
    , elmDefinition @ClientMessage
    , elmDecoderDefinition @JSON.Value @ServerMessage
    , elmEncoderDefinition @JSON.Value @ClientMessage
    ]
