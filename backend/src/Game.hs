{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Game
  ( shuffledDeck
  , drawCards
  , checkSet
  , setPossible
  , Game
  , GameID(..)
  , GameDesc
  , Settings
  , Player
  , PlayerID(..)
  , newGame
  , startGame
  , resetGame
  , endGame
  , newPlayer
  , addPlayer
  , removePlayer
  , changeSettings
  , pickCards
  , requestCards
  , getGameDesc
  , getPlayer
  , getAllPlayers
  , getPlayerID
  , getGameID
  , getOwner
  , isOrphaned
  , isEnded
  , getElmDefinitions
  ) where

-- Imports

import GHC.Generics
import Data.List
import Data.Text (Text)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Aeson (FromJSON, ToJSON, ToJSONKey, toJSON, parseJSON)
import Data.Aeson.Types (toJSONKeyText)
import qualified Data.Aeson as JSON
import qualified Data.Map.Strict as Map
import System.Random
import Data.Array.IO
import Data.Maybe
import Control.Monad
import Language.Haskell.To.Elm
import qualified Generics.SOP as SOP
import qualified Language.Elm.Type as Type
import qualified Language.Elm.Expression as Expression
import Language.Elm.Definition (Definition)

-- Data Types

data Game
  = Initialized
    { gamePlayers :: PlayerDict
    , gameID :: GameID
    , gameOwner :: Player
    , gameSettings :: Settings
    , gameName :: Text
    }
  | Active
    { gamePlayers :: PlayerDict
    , gameDeck :: Deck
    , gameBoard :: Board
    , gameID :: GameID
    , gameOwner :: Player
    , gameSettings :: Settings
    , gameBlockedPlayers :: PlayerGroup
    , gameRequestedCards :: PlayerGroup
    , gameName :: Text
    }
  | Ended
    { gamePlayers :: PlayerDict
    , gameWinner :: PlayerID
    , gameID :: GameID
    , gameOwner :: Player
    , gameSettings :: Settings
    , gameName :: Text
    }
  | Orphaned
    { gameID :: GameID
    , gameName :: Text
    }
  deriving (Generic, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Game where
  elmDefinition = Just $ deriveElmTypeDefinition @Game defaultOptions "Api.Game.Game"

instance HasElmDecoder JSON.Value Game where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Game defaultOptions JSON.defaultOptions "Api.Game.gameDecoder"

newtype PlayerDict = PlayerDict (Map.Map PlayerID Player)
  deriving (Generic, ToJSON)

instance HasElmType PlayerDict where
  elmType = Type.App (Type.App "Dict.Dict" (elmType @Text)) (elmType @Player)

instance HasElmDecoder JSON.Value PlayerDict where
  elmDecoder = Expression.App "Json.Decode.dict" (elmDecoder @JSON.Value @Player)

newtype PlayerGroup = PlayerGroup (Set PlayerID)
  deriving (Generic, ToJSON)

instance HasElmType PlayerGroup where
  elmType = Type.App "List.List" (elmType @PlayerID)

instance HasElmDecoder JSON.Value PlayerGroup where
  elmDecoder = Expression.App "Json.Decode.list" (elmDecoder @JSON.Value @PlayerID)

newtype GameID = GameID Text
  deriving (Generic, Show, Eq, Ord, ToJSONKey, SOP.Generic, SOP.HasDatatypeInfo)

instance ToJSON GameID where
  toJSON (GameID gid) = toJSON gid

instance FromJSON GameID where
  parseJSON = JSON.withText "ID" $ \t -> do
    gid <- parseJSON (JSON.String t)
    return $ GameID gid

instance HasElmType GameID where
  elmType = "String.String"

instance HasElmEncoder JSON.Value GameID where
  elmEncoder = "Json.Encode.string"

instance HasElmDecoder JSON.Value GameID where
  elmDecoder = "Json.Decode.string"

data GameDesc = GameDesc
  { gameDescID :: GameID
  , gameDescNumPlayers :: Int
  , gameDescState :: Text
  , gameDescName :: Text
  }
  deriving (Generic, Show, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType GameDesc where
  elmDefinition = Just $ deriveElmTypeDefinition @GameDesc defaultOptions "Api.Game.GameDesc"

instance HasElmDecoder JSON.Value GameDesc where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @GameDesc defaultOptions JSON.defaultOptions "Api.Game.gameDescDecoder"

data Settings = Settings
  { autoEndGame :: Bool
  , cardRequestRule :: CardRequestRule
  , wrongPickRule :: WrongPickRule
  }
  deriving (Generic, Show, ToJSON, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Settings where
  elmDefinition = Just $ deriveElmTypeDefinition @Settings defaultOptions "Api.Game.Settings"

instance HasElmEncoder JSON.Value Settings where
  elmEncoderDefinition =
    Just $ deriveElmJSONEncoder @Settings defaultOptions JSON.defaultOptions "Api.Game.settingsEncoder"

instance HasElmDecoder JSON.Value Settings where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Settings defaultOptions JSON.defaultOptions "Api.Game.settingsDecoder"

data CardRequestRule
  = SingleRequest
  | PercentageRequest Double
  deriving (Generic, Show, ToJSON, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType CardRequestRule where
  elmDefinition = Just $ deriveElmTypeDefinition @CardRequestRule defaultOptions "Api.Game.CardRequestRule"

instance HasElmEncoder JSON.Value CardRequestRule where
  elmEncoderDefinition =
    Just $ deriveElmJSONEncoder @CardRequestRule defaultOptions JSON.defaultOptions "Api.Game.cardRequestRuleEncoder"

instance HasElmDecoder JSON.Value CardRequestRule where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @CardRequestRule defaultOptions JSON.defaultOptions "Api.Game.cardRequestRuleDecoder"

data WrongPickRule
  = NoAction
  | NegativePoint
  | BlockedForRound
  deriving (Generic, Show, ToJSON, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType WrongPickRule where
  elmDefinition = Just $ deriveElmTypeDefinition @WrongPickRule defaultOptions "Api.Game.WrongPickRule"

instance HasElmEncoder JSON.Value WrongPickRule where
  elmEncoderDefinition =
    Just $ deriveElmJSONEncoder @WrongPickRule defaultOptions JSON.defaultOptions "Api.Game.wrongPickRuleEncoder"

instance HasElmDecoder JSON.Value WrongPickRule where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @WrongPickRule defaultOptions JSON.defaultOptions "Api.Game.wrongPickRuleDecoder"

data Player = Player
  { playerName :: Text
  , playerPoints :: Int
  , playerID :: PlayerID
  }
  deriving (Generic, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Player where
  elmDefinition = Just $ deriveElmTypeDefinition @Player defaultOptions "Api.Game.Player"

instance HasElmDecoder JSON.Value Player where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Player defaultOptions JSON.defaultOptions "Api.Game.playerDecoder"

newtype PlayerID = PlayerID Text
  deriving (Generic, Show, Eq, Ord, SOP.Generic, SOP.HasDatatypeInfo)

instance ToJSON PlayerID where
  toJSON (PlayerID pid) = toJSON pid

instance ToJSONKey PlayerID where
  toJSONKey = toJSONKeyText (\(PlayerID pid) -> pid)

instance HasElmType PlayerID where
  elmType = "String.String"

instance HasElmDecoder JSON.Value PlayerID where
  elmDecoder = "Json.Decode.string"

data Deck = Deck [Card]

instance ToJSON Deck where
  toJSON (Deck cards) = toJSON $ length cards

instance HasElmType Deck where
  elmType = "Basics.Int"

instance HasElmDecoder JSON.Value Deck where
  elmDecoder = "Json.Decode.int"

data Board
  = Normal Card Card Card Card Card Card Card Card Card Card Card Card
  | Added1 Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card
  | Added2 Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card
  | Added3 Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card Card
  | Reduced1 Card Card Card Card Card Card Card Card Card
  | Reduced2 Card Card Card Card Card Card
  | Reduced3 Card Card Card
  | InvalidBoard
  deriving (Generic, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Board where
  elmDefinition = Just $ deriveElmTypeDefinition @Board defaultOptions "Api.Game.Board"

instance HasElmDecoder JSON.Value Board where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Board defaultOptions JSON.defaultOptions "Api.Game.boardDecoder"

data Color
  = Green
  | Red
  | Violet
  deriving (Generic, Eq, Show, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Color where
  elmDefinition = Just $ deriveElmTypeDefinition @Color defaultOptions "Api.Game.Color"

instance HasElmDecoder JSON.Value Color where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Color defaultOptions JSON.defaultOptions "Api.Game.colorDecoder"

data Shape
  = Oval
  | Diamond
  | Wave
  deriving (Generic, Eq, Show, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Shape where
  elmDefinition = Just $ deriveElmTypeDefinition @Shape defaultOptions "Api.Game.Shape"

instance HasElmDecoder JSON.Value Shape where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Shape defaultOptions JSON.defaultOptions "Api.Game.shapeDecoder"

data Fill
  = Full
  | Half
  | Empty
  deriving (Generic, Eq, Show, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Fill where
  elmDefinition = Just $ deriveElmTypeDefinition @Fill defaultOptions "Api.Game.Fill"

instance HasElmDecoder JSON.Value Fill where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Fill defaultOptions JSON.defaultOptions "Api.Game.fillDecoder"

data Number
  = One
  | Two
  | Three
  deriving (Generic, Eq, Show, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Number where
  elmDefinition = Just $ deriveElmTypeDefinition @Number defaultOptions "Api.Game.Number"

instance HasElmDecoder JSON.Value Number where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Number defaultOptions JSON.defaultOptions "Api.Game.numberDecoder"

data Card = Card
  { color :: Color
  , shape :: Shape
  , fill :: Fill
  , number :: Number
  }
  deriving (Generic, Eq, Show, ToJSON, SOP.Generic, SOP.HasDatatypeInfo)

instance HasElmType Card where
  elmDefinition = Just $ deriveElmTypeDefinition @Card defaultOptions "Api.Game.Card"

instance HasElmDecoder JSON.Value Card where
  elmDecoderDefinition =
    Just $ deriveElmJSONDecoder @Card defaultOptions JSON.defaultOptions "Api.Game.cardDecoder"

-- Generate Card Deck

shuffledDeck :: IO Deck
shuffledDeck = do
  cards <- shuffle $ map intToCard [0..80]
  return $ Deck cards

shuffle :: [a] -> IO [a]
shuffle xs = do
    ar <- makeArray n xs
    forM [1..n] $ \i -> do
      j <- randomRIO (i, n)
      vi <- readArray ar i
      vj <- readArray ar j
      writeArray ar j vi
      return vj
  where
    n = length xs
    makeArray :: Int -> [a] -> IO (IOArray Int a)
    makeArray ny ys = newListArray (1, ny) ys

-- Card Manipulation

drawCards :: Int -> Deck -> ([Card], Deck)
drawCards i (Deck d) = (take i d, Deck $ drop i d)

-- Check Set

-- Check if three cards form a set
checkSet :: Card -> Card -> Card -> Bool
checkSet c1 c2 c3 =
  if c1 == c2 || c2 == c3 || c3 == c1
    then False
    else if propSetPossible (color c1) (color c2) (color c3)
      && propSetPossible (shape c1) (shape c2) (shape c3)
      && propSetPossible (fill c1) (fill c2) (fill c3)
      && propSetPossible (number c1) (number c2) (number c3)
      then True
      else False

-- Check if three properties leave opportunity for a set
propSetPossible :: (Eq a) => a -> a -> a -> Bool
propSetPossible p1 p2 p3 =
  if p1 == p2 && p2 == p3
    || p1 /= p2 && p2 /= p3 && p3 /= p1
    then True
    else False

-- Check if a set can be found in a deck of cards
setPossible :: [Card] -> Bool
setPossible [] = False
setPossible [_] = False
setPossible [_, _] = False
setPossible [c1, c2, c3] = checkSet c1 c2 c3
setPossible cs =
  if length cs > 20
    then True
    else 
      or $ map (\xs -> checkSet (head xs) (head (tail xs)) (xs !! 2))
        $ filter ((3==).length.nub) $ mapM (const cs) ([1..3] :: [Int])

-- Int to Card conversion for generating card stack

intToCard :: Int -> Card
intToCard i =
  Card
    { color = intToColor i
    , shape = intToShape i
    , fill = intToFill i
    , number = intToNumber i
    }

intToColor :: Int -> Color
intToColor i = nOrderMap 3 [Green, Red, Violet] i

intToShape :: Int -> Shape
intToShape i = nOrderMap 2 [Oval, Diamond, Wave] i

intToFill :: Int -> Fill
intToFill i = nOrderMap 1 [Full, Half, Empty] i

intToNumber :: Int -> Number
intToNumber i = nOrderMap 0 [One, Two, Three] i

nOrderMap :: Int -> [a] -> Int -> a
nOrderMap order options i =
  let
    base = length options
    m = (i `div` base^order) `mod` base
  in
    options !! m

-- Player

newPlayer :: Text -> PlayerID -> Player
newPlayer name pid =
  Player
    { playerName = name
    , playerPoints = 0
    , playerID = pid
    }

-- Game manipulation

defaultSettings :: Settings
defaultSettings = Settings
  { autoEndGame = True
  , cardRequestRule = SingleRequest
  , wrongPickRule = NoAction
  }

newGame :: GameID -> Player -> Text -> IO Game
newGame gid owner name = do
  return Initialized
    { gamePlayers = PlayerDict Map.empty
    , gameID = gid
    , gameOwner = owner
    , gameSettings = defaultSettings
    , gameName = name
    }

startGame :: Game -> IO Game
startGame game =
  case game of
    Initialized { gamePlayers = players, gameID = gid, gameOwner = owner, gameSettings = settings, gameName = name } -> do
      newDeck <- shuffledDeck
      let (board, deck) = newBoard newDeck
      return Active
        { gamePlayers = players
        , gameDeck = deck
        , gameBoard = board
        , gameID = gid
        , gameOwner = owner
        , gameSettings = settings
        , gameBlockedPlayers = PlayerGroup Set.empty
        , gameRequestedCards = PlayerGroup Set.empty
        , gameName = name
        }
    Ended { gamePlayers = players, gameID = gid, gameOwner = owner, gameSettings = settings, gameName = name } -> do
      newDeck <- shuffledDeck
      let (board, deck) = newBoard newDeck
      return Active
        { gamePlayers = players
        , gameDeck = deck
        , gameBoard = board
        , gameID = gid
        , gameOwner = owner
        , gameSettings = settings
        , gameBlockedPlayers = PlayerGroup Set.empty
        , gameRequestedCards = PlayerGroup Set.empty
        , gameName = name
        }
    _ -> return game

endGame :: Game -> Game
endGame game =
  Ended
    { gameOwner = gameOwner game
    , gamePlayers = gamePlayers game
    , gameID = gameID game
    , gameSettings = gameSettings game
    , gameWinner = getWinner game
    , gameName = gameName game
    }

resetGame :: Game -> Game
resetGame game =
  Initialized
    { gamePlayers = gamePlayers game
    , gameID = gameID game
    , gameOwner = gameOwner game
    , gameSettings = gameSettings game
    , gameName = gameName game
    }

addPlayer :: Player -> Game -> Game
addPlayer player game =
  let (PlayerDict m) = gamePlayers game
  in game { gamePlayers = PlayerDict $ Map.insert (playerID player) player m }

removePlayer :: PlayerID -> Game -> Game
removePlayer pid game =
  let (PlayerDict m) = gamePlayers game
  in if Map.member pid m
    then
      game { gamePlayers = PlayerDict $ Map.delete pid m }
    else
      if Map.null m
        then
          Orphaned { gameID = gameID game, gameName = gameName game }
        else
          game
            { gamePlayers = PlayerDict $ Map.fromList $ tail $ Map.toList m
            , gameOwner = snd $ head $ Map.toList m
            }

changeSettings :: Settings -> Game -> Game
changeSettings settings game =
  game { gameSettings = settings }

pickCards :: (Int, Int, Int) -> PlayerID -> Game -> Game
pickCards picks@(p1, p2, p3) pid game =
  let (PlayerGroup blocked) = gameBlockedPlayers game
  in case game of
    Active {} -> case elem pid blocked of
      True -> game
      False ->
        let cards = boardToCards $ gameBoard game
        in case checkSet (cards !! p1) (cards !! p2) (cards !! p3) of
          False -> case wrongPickRule $ gameSettings game of
            -- Wrong Pick
            NoAction -> game
            NegativePoint -> playerModPoints game pid (-1)
            BlockedForRound -> game { gameBlockedPlayers = PlayerGroup $ Set.insert pid blocked }
          True ->
            -- Correct Pick
            let (draw, newDeck) = drawCards 3 $ gameDeck game
                board = gameBoard game
            in
              let modBoard = case draw of
                              [c1, c2, c3] -> case board of
                                Normal {} -> replaceOnBoard picks (c1, c2, c3) board
                                Added1 {} -> removeFromBoard picks board
                                Added2 {} -> removeFromBoard picks board
                                Added3 {} -> removeFromBoard picks board
                                Reduced1 {} -> replaceOnBoard picks (c1, c2, c3) board
                                Reduced2 {} -> replaceOnBoard picks (c1, c2, c3) board
                                Reduced3 {} -> replaceOnBoard picks (c1, c2, c3) board
                                _ -> board
                              [] -> removeFromBoard picks board
                              _ -> board
                  possibleSet = setPossible $ boardToCards modBoard
                  modGame = (playerModPoints game pid 1)
                    { gameDeck = newDeck
                    , gameBoard = modBoard
                    , gameBlockedPlayers = PlayerGroup Set.empty
                    , gameRequestedCards = PlayerGroup Set.empty
                    }
              in case modBoard of
                InvalidBoard -> endGame modGame
                _ -> case possibleSet of
                  True -> modGame
                  False -> case autoEndGame $ gameSettings game of
                    True -> endGame modGame
                    False -> modGame
    _ -> game

playerModPoints :: Game -> PlayerID -> Int -> Game
playerModPoints game pid modi =
  case (playerID $ gameOwner game) == pid of
    True -> game { gameOwner = (gameOwner game) { playerPoints = modi + (playerPoints $ gameOwner game) } }
    False ->
      let (PlayerDict m) = gamePlayers game
      in case Map.lookup pid m of
        Nothing -> game
        Just player -> game { gamePlayers = PlayerDict $ Map.insert pid
          player { playerPoints = modi + (playerPoints player) } m }

requestCards :: PlayerID -> Game -> Game
requestCards pid game =
  let (PlayerGroup requested) = gameRequestedCards game
  in case cardRequestRule $ gameSettings game of
    SingleRequest -> addCards game
    PercentageRequest perc -> case (fromIntegral $ 1 + (length requested))
      > (perc * (fromIntegral $ length $ getAllPlayers game)) of
        True -> addCards game
        False -> game { gameRequestedCards = PlayerGroup $ Set.insert pid $ requested }

addCards :: Game -> Game
addCards game =
  let (draw, newDeck) = drawCards 3 $ gameDeck game
  in case gameBoard game of
    Added3 {} -> game
    _ -> game { gameBoard = cardsToBoard $ (boardToCards $ gameBoard game) ++ draw
              , gameDeck = newDeck
              , gameRequestedCards = PlayerGroup Set.empty
              , gameBlockedPlayers = PlayerGroup Set.empty
              }

getPlayer :: PlayerID -> Game -> Maybe Player
getPlayer pid game =
  if pid == (playerID $ gameOwner game) then
    Just $ gameOwner game
  else
    let (PlayerDict m) = gamePlayers game
    in Map.lookup pid m

getPlayerID :: Player -> PlayerID
getPlayerID player =
  playerID player

getAllPlayers :: Game -> [Player]
getAllPlayers game =
  let (PlayerDict m) = gamePlayers game
  in gameOwner game : (Map.elems m)

getGameDesc :: Game -> GameDesc
getGameDesc game =
  GameDesc
    { gameDescID = gameID game
    , gameDescNumPlayers = length $ getAllPlayers game
    , gameDescState =
      case game of
        Initialized {} -> "Initialized"
        Active {} -> "Active"
        Ended {} -> "Ended"
        Orphaned {} -> "Orphaned"
    , gameDescName = gameName game
    }

getGameID :: Game -> GameID
getGameID game =
  gameID game

getOwner :: Game -> Player
getOwner game =
  gameOwner game

getWinner :: Game -> PlayerID
getWinner game =
  case game of
    Ended {} -> gameWinner game
    _ -> playerID $ head $ sortOn playerPoints $ getAllPlayers game

isOrphaned :: Game -> Bool
isOrphaned game =
  case game of
    Orphaned {} -> True
    _ -> False

isEnded :: Game -> Bool
isEnded game =
  case game of
    Ended {} -> True
    _ -> False

-- Board manipulation

newBoard :: Deck -> (Board, Deck)
newBoard deck =
  let (draw, newDeck) = drawCards 12 deck
  in (cardsToBoard draw, newDeck)

replaceOnBoard :: (Int, Int, Int) -> (Card, Card, Card) -> Board -> Board
replaceOnBoard (p1, p2, p3) (c1, c2, c3) board =
  cardsToBoard $ (repl p1 c1 . repl p2 c2 . repl p3 c3) $ boardToCards board
  where
    repl = \i x xs ->
      let (st, _:en) = splitAt i xs
      in st ++ x : en

removeFromBoard :: (Int, Int, Int) -> Board -> Board
removeFromBoard (p1, p2, p3) board =
  cardsToBoard $ filter (\c -> c /= c1 && c /= c2 && c /= c3) cards
  where
    cards = boardToCards board
    c1 = (cards !! p1)
    c2 = (cards !! p2)
    c3 = (cards !! p3)

cardsToBoard :: [Card] -> Board
cardsToBoard cards =
  case length cards of
    12 -> Normal (head cards) (cards !! 1) (cards !! 2) (cards !! 3) (cards !! 4) (cards !! 5)
      (cards !! 6) (cards !! 7) (cards !! 8) (cards !! 9) (cards !! 10) (cards !! 11)
    9 -> Reduced1 (head cards) (cards !! 1) (cards !! 2) (cards !! 3) (cards !! 4) (cards !! 5)
      (cards !! 6) (cards !! 7) (cards !! 8)
    6 -> Reduced2 (head cards) (cards !! 1) (cards !! 2) (cards !! 3) (cards !! 4) (cards !! 5)
    3 -> Reduced3 (head cards) (cards !! 1) (cards !! 2)
    15 -> Added1 (head cards) (cards !! 1) (cards !! 2) (cards !! 3) (cards !! 4) (cards !! 5)
      (cards !! 6) (cards !! 7) (cards !! 8) (cards !! 9) (cards !! 10) (cards !! 11)
      (cards !! 12) (cards !! 13) (cards !! 14)
    18 -> Added2 (head cards) (cards !! 1) (cards !! 2) (cards !! 3) (cards !! 4) (cards !! 5)
      (cards !! 6) (cards !! 7) (cards !! 8) (cards !! 9) (cards !! 10) (cards !! 11)
      (cards !! 12) (cards !! 13) (cards !! 14) (cards !! 15) (cards !! 16) (cards !! 17)
    21 -> Added3 (head cards) (cards !! 1) (cards !! 2) (cards !! 3) (cards !! 4) (cards !! 5)
      (cards !! 6) (cards !! 7) (cards !! 8) (cards !! 9) (cards !! 10) (cards !! 11)
      (cards !! 12) (cards !! 13) (cards !! 14) (cards !! 15) (cards !! 16) (cards !! 17)
      (cards !! 18) (cards !! 19) (cards !! 20)
    _ -> InvalidBoard

boardToCards :: Board -> [Card]
boardToCards board =
  case board of
    Normal c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 -> [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12]
    Reduced1 c1 c2 c3 c4 c5 c6 c7 c8 c9 -> [c1, c2, c3, c4, c5, c6, c7, c8, c9]
    Reduced2 c1 c2 c3 c4 c5 c6 -> [c1, c2, c3, c4, c5, c6]
    Reduced3 c1 c2 c3 -> [c1, c2, c3]
    Added1 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 c13 c14 c15 ->
      [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15]
    Added2 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 c13 c14 c15 c16 c17 c18 ->
      [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18]
    Added3 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 c13 c14 c15 c16 c17 c18 c19 c20 c21 ->
      [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20, c21]
    InvalidBoard -> []

-- Generate Elm Code

getElmDefinitions :: [Definition]
getElmDefinitions =
  catMaybes
    [ elmDefinition @Game
    , elmDefinition @GameID
    , elmDefinition @GameDesc
    , elmDefinition @Player
    , elmDefinition @PlayerID
    , elmDefinition @Settings
    , elmDefinition @CardRequestRule
    , elmDefinition @WrongPickRule
    , elmDefinition @Board
    , elmDefinition @Card
    , elmDefinition @Color
    , elmDefinition @Shape
    , elmDefinition @Fill
    , elmDefinition @Number
    , elmDecoderDefinition @JSON.Value @Game
    , elmDecoderDefinition @JSON.Value @GameID
    , elmDecoderDefinition @JSON.Value @GameDesc
    , elmDecoderDefinition @JSON.Value @Player
    , elmDecoderDefinition @JSON.Value @PlayerID
    , elmDecoderDefinition @JSON.Value @Settings
    , elmDecoderDefinition @JSON.Value @CardRequestRule
    , elmDecoderDefinition @JSON.Value @WrongPickRule
    , elmDecoderDefinition @JSON.Value @Board
    , elmDecoderDefinition @JSON.Value @Card
    , elmDecoderDefinition @JSON.Value @Color
    , elmDecoderDefinition @JSON.Value @Shape
    , elmDecoderDefinition @JSON.Value @Fill
    , elmDecoderDefinition @JSON.Value @Number
    , elmEncoderDefinition @JSON.Value @GameID
    , elmEncoderDefinition @JSON.Value @Settings
    , elmEncoderDefinition @JSON.Value @CardRequestRule
    , elmEncoderDefinition @JSON.Value @WrongPickRule
    ]
