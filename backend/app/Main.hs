{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import qualified Server
import qualified Game
import System.Directory
import System.Console.ArgParser
import Data.Foldable
import qualified Data.Text as T
import qualified Data.HashMap.Lazy as HashMap
import qualified Control.Logging as Log
import Data.Text.Prettyprint.Doc (Doc)
import qualified Data.Text.Prettyprint.Doc as PrettyPrint
import Data.Text.Prettyprint.Doc.Render.String as PrettyString
import qualified Language.Elm.Name as Name
import qualified Language.Elm.Pretty as Pretty
import qualified Language.Elm.Simplification as Simplification

data CliArgs
  = RunArgs String Int String String
  | GenerateElm String

cliParser :: IO (CmdLnInterface CliArgs)
cliParser = mkSubParser
  [ ("run", mkDefaultApp
    (RunArgs `parsedBy`
    reqPos "address" `andBy`
    optPos 9160 "port" `andBy`
    optPos "/" "path" `andBy`
    optPos "RandomSalt" "salt") "run")
  , ("generate-elm", mkDefaultApp (GenerateElm `parsedBy` optPos "elm-code" "folderName") "generate-elm")
  ]

main :: IO ()
main = do
  interface <- cliParser
  runApp interface mainWithArgs

mainWithArgs :: CliArgs -> IO ()
mainWithArgs args =
  case args of
    RunArgs addr port path salt -> Log.withStdoutLogging
      $ Server.startServer (T.pack addr) port (T.pack path) (T.pack salt)
    GenerateElm folderName -> generateElm folderName

generateElm :: String -> IO ()
generateElm folderName = do
  let
    definitions = Simplification.simplifyDefinition <$> Game.getElmDefinitions ++ Server.getElmDefinitions
    modules = Pretty.modules definitions
  createDirectoryIfMissing True folderName
  setCurrentDirectory folderName
  forM_ (HashMap.toList modules) $ \(moduleName, contents) -> writeModule moduleName contents

writeModule :: Name.Module -> Doc ann -> IO ()
writeModule moduleName contents = do
  createDirectoryIfMissing True $ T.unpack $ T.intercalate "/" $ init moduleName
  writeFile (T.unpack $ T.append (T.intercalate "/" moduleName) ".elm") $ PrettyString.renderString
    $ PrettyPrint.layoutPretty PrettyPrint.defaultLayoutOptions contents
