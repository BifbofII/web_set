# Web Set

A browser based implementation of the card game "Set".

The application is divided into a backend written in Haskell and a frontend written in Elm.

## Current Development State

- [x] Backend
  - [x] Websocket interface
  - [x] Service start with CLI parameters
- [ ] Frontend
  - [x] Connection to backend
  - [ ] URL routing
  - [ ] Pages
    - [x] Lobby page
      - [x] Join game
      - [x] Create game
    - [x] Game page
      - [x] Board
      - [x] Player list
      - [x] Settings
      - [x] Result
  - [x] Helpful Error messages
- [x] Automatic Elm code generation for Websocket interface datatypes
- [ ] Continuous integration to build Docker images