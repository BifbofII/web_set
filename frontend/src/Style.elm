module Style exposing
    ( addButton
    , addButtonColor
    , addButtonIcon
    , black
    , blue
    , buttonBorder
    , buttonPadding
    , buttonRounded
    , buttonSize
    , cancleButton
    , cancleButtonColor
    , cardBorder
    , cardBorderColor
    , cardBorderColorHover
    , cardBorderColorSelected
    , cardBorderColorSelectedHover
    , cardColor
    , cardGreenColor
    , cardMaxSymbHeight
    , cardPadding
    , cardRedColor
    , cardRounding
    , cardSpacing
    , cardVioletColor
    , closeButton
    , closeButtonColor
    , closeButtonIcon
    , contentMaxWidth
    , contentPadding
    , darkGrey
    , darken
    , errorPadding
    , gameBoardBorder
    , gameBoardColor
    , gameBoardPadding
    , gameBoardPortion
    , gameBoardRounding
    , gameBoardTitleSize
    , gameDescBorder
    , gameDescColor
    , gameDescColorSelected
    , gameDescHeight
    , gameDescPadding
    , gameListBorder
    , gameListColor
    , gameListHeight
    , gameListPadding
    , gameListRounding
    , gameListTitleSize
    , gameListWidth
    , gameSpacing
    , green
    , headerFontWeight
    , headerFooterColor
    , headerFooterFontColor
    , headerFooterHeight
    , headerFooterMiddlePortion
    , headerFooterPadding
    , imageButton
    , imageButtonWithColor
    , infoBgColor
    , infoBox
    , lightGrey
    , linkStyle
    , mediumGrey
    , messageBox
    , messageBoxBorder
    , messageBoxFontColor
    , messageBoxPadding
    , messageBoxRounding
    , okButton
    , playerBorder
    , playerColor
    , playerColorSelf
    , playerHeight
    , playerListBorder
    , playerListColor
    , playerListPadding
    , playerListRounding
    , playerListTitleSize
    , playerPadding
    , popupBorder
    , popupColor
    , popupPadding
    , popupTitleHeight
    , red
    , settingsButton
    , settingsButtonColor
    , settingsButtonIcon
    , settingsButtonPadding
    , textButton
    , violet
    , warningBgColor
    , warningBox
    , white
    )

import Element
    exposing
        ( Attribute
        , Color
        , Element
        , Length
        , column
        , el
        , fill
        , focused
        , fromRgb
        , height
        , image
        , mouseOver
        , none
        , padding
        , px
        , rgb255
        , spacing
        , text
        , toRgb
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input



-- DEFAULTS


defaultBorder : Int
defaultBorder =
    5



-- COLORS


green : Color
green =
    rgb255 20 190 109


red : Color
red =
    rgb255 224 68 68


violet : Color
violet =
    rgb255 153 51 255


blue : Color
blue =
    rgb255 18 144 174


white : Color
white =
    rgb255 255 255 255


black : Color
black =
    rgb255 0 0 0


darkGrey : Color
darkGrey =
    rgb255 64 64 64


mediumGrey : Color
mediumGrey =
    rgb255 150 150 150


lightGrey : Color
lightGrey =
    rgb255 184 184 184



-- CONTENT


contentPadding : Int
contentPadding =
    40


contentMaxWidth : Int
contentMaxWidth =
    1200



-- HEADER / FOOTER


headerFooterHeight : Int
headerFooterHeight =
    50


headerFooterColor : Color
headerFooterColor =
    darkGrey


headerFooterPadding : Int
headerFooterPadding =
    5


headerFooterMiddlePortion : Int
headerFooterMiddlePortion =
    4


headerFooterFontColor : Color
headerFooterFontColor =
    white


headerFontWeight : Attribute msg
headerFontWeight =
    Font.extraBold



-- MESSAGE BOX


messageBoxPadding : Int
messageBoxPadding =
    10


messageBoxBorder : Int
messageBoxBorder =
    defaultBorder


messageBoxFontColor : Color
messageBoxFontColor =
    rgb255 220 220 220


messageBoxRounding : Int
messageBoxRounding =
    10


warningBgColor : Color
warningBgColor =
    rgb255 204 0 0


infoBgColor : Color
infoBgColor =
    rgb255 0 102 204



-- LINKS


linkStyle : List (Attribute msg)
linkStyle =
    [ Font.italic
    , Font.underline
    ]



-- GAME LIST


gameListHeight : Int
gameListHeight =
    700


gameListWidth : Int
gameListWidth =
    600


gameListColor : Color
gameListColor =
    mediumGrey


gameListBorder : Int
gameListBorder =
    defaultBorder


gameListRounding : Int
gameListRounding =
    10


gameListPadding : Int
gameListPadding =
    10


gameListTitleSize : Int
gameListTitleSize =
    50



-- GAME DESC


gameDescHeight : Length
gameDescHeight =
    px 100


gameDescColor : Color
gameDescColor =
    blue


gameDescColorSelected : Color
gameDescColorSelected =
    rgb255 75 171 193


gameDescBorder : Int
gameDescBorder =
    defaultBorder


gameDescPadding : Int
gameDescPadding =
    20



-- GAME


gameSpacing : Int
gameSpacing =
    20



-- GAME BOARD


gameBoardPortion : Int
gameBoardPortion =
    4


gameBoardColor : Color
gameBoardColor =
    mediumGrey


gameBoardBorder : Int
gameBoardBorder =
    defaultBorder


gameBoardRounding : Int
gameBoardRounding =
    10


gameBoardPadding : Int
gameBoardPadding =
    10


gameBoardTitleSize : Int
gameBoardTitleSize =
    50



-- PLAYER LIST


playerListColor : Color
playerListColor =
    mediumGrey


playerListBorder : Int
playerListBorder =
    defaultBorder


playerListRounding : Int
playerListRounding =
    10


playerListPadding : Int
playerListPadding =
    10


playerListTitleSize : Int
playerListTitleSize =
    30



-- PLAYER


playerHeight : Length
playerHeight =
    px 100


playerColor : Color
playerColor =
    blue


playerColorSelf : Color
playerColorSelf =
    rgb255 75 171 193


playerBorder : Int
playerBorder =
    defaultBorder


playerPadding : Int
playerPadding =
    20



-- CARD


cardColor : Color
cardColor =
    white


cardBorder : Int
cardBorder =
    defaultBorder


cardBorderColor : Color
cardBorderColor =
    black


cardBorderColorHover : Color
cardBorderColorHover =
    rgb255 75 171 193


cardBorderColorSelected : Color
cardBorderColorSelected =
    green


cardBorderColorSelectedHover : Color
cardBorderColorSelectedHover =
    darken 0.3 green


cardRounding : Int
cardRounding =
    20


cardPadding : Int
cardPadding =
    25


cardSpacing : Int
cardSpacing =
    5


cardGreenColor : Color
cardGreenColor =
    green


cardRedColor : Color
cardRedColor =
    red


cardVioletColor : Color
cardVioletColor =
    violet


cardMaxSymbHeight : Int
cardMaxSymbHeight =
    50



-- POPUPS


popupColor : Color
popupColor =
    lightGrey


popupBorder : Int
popupBorder =
    defaultBorder


popupTitleHeight : Length
popupTitleHeight =
    px 50


popupPadding : Int
popupPadding =
    10



-- ERROR MESSAGES


errorPadding : Int
errorPadding =
    20



-- BUTTONS


buttonSize : Length
buttonSize =
    px 40


buttonBorder : Int
buttonBorder =
    5


buttonPadding : Int
buttonPadding =
    5


buttonRounded : Int
buttonRounded =
    5


closeButtonIcon : String
closeButtonIcon =
    "icons/close.svg"


closeButtonColor : Color
closeButtonColor =
    red


closeButton : msg -> Element msg
closeButton msg =
    imageButtonWithColor msg closeButtonColor 0 closeButtonIcon


addButtonIcon : String
addButtonIcon =
    "icons/add.svg"


addButtonColor : Color
addButtonColor =
    green


addButton : msg -> Element msg
addButton msg =
    imageButtonWithColor msg addButtonColor 0 addButtonIcon


settingsButtonIcon : String
settingsButtonIcon =
    "icons/settings.svg"


settingsButtonColor : Color
settingsButtonColor =
    lightGrey


settingsButtonPadding : Int
settingsButtonPadding =
    2


settingsButton : msg -> Element msg
settingsButton msg =
    imageButtonWithColor msg settingsButtonColor settingsButtonPadding settingsButtonIcon


imageButtonWithColor : msg -> Color -> Int -> String -> Element msg
imageButtonWithColor msg color pad img =
    el
        [ Background.color color
        , Border.rounded buttonRounded
        , mouseOver [ Background.color <| darken 0.2 color ]
        , focused [ Background.color <| darken 0.2 color ]
        ]
    <|
        imageButton msg pad img


imageButton : msg -> Int -> String -> Element msg
imageButton msg pad img =
    Input.button
        [ height buttonSize
        , width buttonSize
        , padding pad
        ]
        { onPress = Just msg
        , label = image [ width fill, height fill ] { src = img, description = "" }
        }


okButtonColor : Color
okButtonColor =
    green


okButton : msg -> Element msg
okButton msg =
    textButton msg okButtonColor "Ok"


cancleButtonColor : Color
cancleButtonColor =
    red


cancleButton : msg -> Element msg
cancleButton msg =
    textButton msg cancleButtonColor "Cancle"


textButton : msg -> Color -> String -> Element msg
textButton msg color t =
    Input.button
        [ Background.color color
        , Border.width buttonBorder
        , Border.color <| darken 0.5 color
        , padding buttonPadding
        , mouseOver [ Background.color <| darken 0.2 color ]
        , focused [ Background.color <| darken 0.2 color ]
        ]
        { onPress = Just msg
        , label = text t
        }



-- HELPERS


darken : Float -> Color -> Color
darken amount color =
    let
        dark =
            -1 * amount + 1

        rgbVal =
            toRgb color
    in
    fromRgb { rgbVal | red = dark * rgbVal.red, green = dark * rgbVal.green, blue = dark * rgbVal.blue }



-- BOXES


warningBox : String -> String -> Element msg
warningBox title message =
    messageBox warningBgColor title message


infoBox : String -> String -> Element msg
infoBox title message =
    messageBox infoBgColor title message


messageBox : Color -> String -> String -> Element msg
messageBox color title message =
    column
        [ padding messageBoxPadding
        , spacing messageBoxPadding
        , Background.color color
        , Border.width messageBoxBorder
        , Border.color <| darken 0.5 color
        , Border.rounded messageBoxRounding
        , Font.color messageBoxFontColor
        ]
        [ el [ Font.bold ] <| text title
        , el [] <| text message
        ]
