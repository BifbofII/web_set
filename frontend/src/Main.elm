module Main exposing (main)

import Api.Game as Game
import Api.Messages as Messages
import Browser
import Browser.Navigation as Nav
import Element
import Json.Decode exposing (decodeString)
import Json.Encode exposing (Value)
import Page
import Popups
import PortFunnel.WebSocket as WS
import PortFunnels exposing (FunnelDict, Handler(..))
import Time
import Url



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL


type alias Model =
    { pfState : PortFunnels.State
    , error : Maybe ( String, String )
    , apiState : ApiModel
    , playerID : Maybe String
    , nickname : Maybe String
    , popup : Maybe ( Popups.Popup, Maybe Msg )
    , backendUrl : String
    }


type ApiModel
    = NotConnected
    | Connecting
    | Connected
    | Idle (List Game.GameDesc)
    | InGame
        { game : Game.Game
        , selected : Maybe ( Int, Maybe Int )
        }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ _ _ =
    ( { pfState = PortFunnels.initialState
      , error = Nothing
      , apiState = NotConnected
      , playerID = Nothing
      , nickname = Nothing
      , popup = Nothing
      , backendUrl = "ws://127.0.0.1:9160"
      }
    , Cmd.none
    )



-- MSG


type Msg
    = UrlChanged Url.Url
    | LinkClicked Browser.UrlRequest
    | PortFunnelMsg Value
    | PageMsg Page.Msg
    | PopupMsg Popups.Msg
    | RemoveError



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model.apiState of
        NotConnected ->
            ( { model | apiState = Connecting }, wsSend model <| WS.makeOpen model.backendUrl )

        _ ->
            case msg of
                UrlChanged _ ->
                    ( model, Cmd.none )

                LinkClicked request ->
                    case request of
                        Browser.Internal _ ->
                            ( model, Cmd.none )

                        Browser.External url ->
                            case model.apiState of
                                InGame _ ->
                                    case model.popup of
                                        Just _ ->
                                            ( model, Cmd.none )

                                        Nothing ->
                                            ( { model | popup = Just ( Popups.LeaveGame url, Nothing ) }, Cmd.none )

                                _ ->
                                    ( model, Nav.load url )

                PortFunnelMsg val ->
                    case PortFunnels.processValue funnelDict val model.pfState model of
                        Err errorMsg ->
                            ( { model | error = Just ( "PortFunnel Error", errorMsg ) }, Cmd.none )

                        Ok res ->
                            res

                RemoveError ->
                    ( { model | error = Nothing }, Cmd.none )

                PageMsg uiMsg ->
                    case uiMsg of
                        Page.JoinGame gid ->
                            case model.popup of
                                Just _ ->
                                    ( model, Cmd.none )

                                Nothing ->
                                    case model.nickname of
                                        Nothing ->
                                            ( { model | popup = Just ( Popups.Name "", Just <| PageMsg <| Page.JoinGame gid ) }
                                            , Cmd.none
                                            )

                                        Just nick ->
                                            ( model, sendClientMessage model <| Messages.ClientJoinGame gid nick )

                        Page.CreateGame ->
                            case model.popup of
                                Just _ ->
                                    ( model, Cmd.none )

                                Nothing ->
                                    let
                                        nick =
                                            case model.nickname of
                                                Nothing ->
                                                    ""

                                                Just n ->
                                                    n
                                    in
                                    ( { model | popup = Just ( Popups.CreateGame nick "", Nothing ) }, Cmd.none )

                        Page.StartGame ->
                            ( model, sendClientMessage model Messages.ClientStartGame )

                        Page.ResetGame ->
                            ( model, sendClientMessage model Messages.ClientResetGame )

                        Page.RequestCards ->
                            ( model, sendClientMessage model Messages.ClientRequestCards )

                        Page.SelectCard ind ->
                            case model.apiState of
                                InGame rec ->
                                    case rec.selected of
                                        Nothing ->
                                            ( { model
                                                | apiState = InGame { rec | selected = Just ( ind, Nothing ) }
                                              }
                                            , Cmd.none
                                            )

                                        Just s1 ->
                                            if Tuple.first s1 == ind then
                                                ( { model
                                                    | apiState = InGame { rec | selected = Nothing }
                                                  }
                                                , Cmd.none
                                                )

                                            else
                                                case Tuple.second s1 of
                                                    Nothing ->
                                                        ( { model
                                                            | apiState = InGame { rec | selected = Just ( Tuple.first s1, Just ind ) }
                                                          }
                                                        , Cmd.none
                                                        )

                                                    Just s2 ->
                                                        if s2 == ind then
                                                            ( { model
                                                                | apiState = InGame { rec | selected = Just ( Tuple.first s1, Nothing ) }
                                                              }
                                                            , Cmd.none
                                                            )

                                                        else
                                                            ( { model | apiState = InGame { rec | selected = Nothing } }
                                                            , sendClientMessage model <| Messages.ClientPickCards ind s2 <| Tuple.first s1
                                                            )

                                _ ->
                                    ( { model
                                        | error =
                                            Just
                                                ( "Invalid Page Command"
                                                , "The received page command is invalid for the current model state"
                                                )
                                      }
                                    , Cmd.none
                                    )

                        Page.OpenSettings ->
                            case model.apiState of
                                InGame rec ->
                                    let
                                        settings =
                                            case rec.game of
                                                Game.Initialized gRec ->
                                                    gRec.gameSettings

                                                Game.Active gRec ->
                                                    gRec.gameSettings

                                                Game.Ended gRec ->
                                                    gRec.gameSettings

                                                _ ->
                                                    { autoEndGame = True
                                                    , cardRequestRule = Game.SingleRequest
                                                    , wrongPickRule = Game.NoAction
                                                    }
                                    in
                                    case model.popup of
                                        Just _ ->
                                            ( model, Cmd.none )

                                        Nothing ->
                                            ( { model | popup = Just ( Popups.GameSettings settings, Nothing ) }, Cmd.none )

                                _ ->
                                    ( { model
                                        | error =
                                            Just
                                                ( "Invalid Page Command"
                                                , "The received page command is invalid for the current model state"
                                                )
                                      }
                                    , Cmd.none
                                    )

                PopupMsg popMsg ->
                    case popMsg of
                        Popups.Close ->
                            ( { model | popup = Nothing }, Cmd.none )

                        Popups.Internal intMsg ->
                            case model.popup of
                                Nothing ->
                                    ( model, Cmd.none )

                                Just ( popup, waitingMsg ) ->
                                    ( { model | popup = Just ( Popups.update popup intMsg, waitingMsg ) }
                                    , Cmd.none
                                    )

                        Popups.RemoveError ->
                            update RemoveError model

                        Popups.Ok ->
                            case model.popup of
                                Nothing ->
                                    ( model, Cmd.none )

                                Just ( popup, waitingMsg ) ->
                                    case popup of
                                        Popups.Name nick ->
                                            case waitingMsg of
                                                Nothing ->
                                                    ( { model | popup = Nothing, nickname = Just nick }, Cmd.none )

                                                Just nextMsg ->
                                                    update nextMsg { model | popup = Nothing, nickname = Just nick }

                                        Popups.CreateGame nick gameName ->
                                            case waitingMsg of
                                                Nothing ->
                                                    ( { model | popup = Nothing, nickname = Just nick }
                                                    , sendClientMessage model <| Messages.ClientCreateGame nick gameName
                                                    )

                                                Just nextMsg ->
                                                    update nextMsg { model | popup = Nothing, nickname = Just nick }

                                        Popups.LeaveGame url ->
                                            ( model, Nav.load url )

                                        Popups.GameSettings settings ->
                                            ( { model | popup = Nothing }
                                            , sendClientMessage model <| Messages.ClientChangeSettings settings
                                            )



-- SUBS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ PortFunnels.subscriptions PortFunnelMsg
        , Time.every 20000 (\_ -> RemoveError)
        ]



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        ( title, page ) =
            Page.view <| getPage model

        popupAttrs =
            case model.popup of
                Nothing ->
                    []

                Just ( popup, _ ) ->
                    [ Element.inFront <| Element.map PopupMsg <| Popups.view popup ]

        errorAttrs =
            case model.error of
                Nothing ->
                    []

                Just error ->
                    [ Element.inFront <| Element.map PopupMsg <| Popups.viewError error ]
    in
    { title = title
    , body =
        [ Element.layout
            (popupAttrs ++ errorAttrs)
          <|
            Element.map PageMsg page
        ]
    }


getPage : Model -> Page.Page
getPage model =
    case model.apiState of
        NotConnected ->
            Page.Connecting

        Connecting ->
            Page.Connecting

        Connected ->
            Page.Connecting

        Idle descList ->
            Page.Lobby descList

        InGame rec ->
            case model.playerID of
                Nothing ->
                    Page.Error "This should not have happened" "We don't know your player ID"

                Just pid ->
                    Page.Game pid rec.game rec.selected



-- PORT FUNNELS


handlers : List (Handler Model Msg)
handlers =
    [ WebSocketHandler socketHandler
    ]


funnelDict : FunnelDict Model Msg
funnelDict =
    PortFunnels.makeFunnelDict handlers getCmdPort


getCmdPort : String -> Model -> (Value -> Cmd Msg)
getCmdPort moduleName _ =
    PortFunnels.getCmdPort PortFunnelMsg moduleName False


wsSend : Model -> WS.Message -> Cmd Msg
wsSend model message =
    WS.send (getCmdPort WS.moduleName model) message



-- WEBSOCKET HANDLER


socketHandler : WS.Response -> PortFunnels.State -> Model -> ( Model, Cmd msg )
socketHandler response state mdl =
    let
        model =
            { mdl | pfState = state }
    in
    case response of
        WS.MessageReceivedResponse { message } ->
            case decodeString Messages.serverMessageDecoder message of
                Ok serverMsg ->
                    ( handleServerMessage model serverMsg, Cmd.none )

                Err _ ->
                    ( { model
                        | error = Just ( "Invalid Server Message", "Recieved an invalid message from the server" )
                      }
                    , Cmd.none
                    )

        WS.ConnectedResponse _ ->
            ( { model | apiState = Connected }, Cmd.none )

        WS.ClosedResponse { code, expected } ->
            if expected then
                ( { model | apiState = NotConnected }, Cmd.none )

            else
                ( { model
                    | error = Just ( "Connection lost unexpectedly", "Code: " ++ WS.closedCodeToString code )
                    , apiState = NotConnected
                  }
                , Cmd.none
                )

        WS.ErrorResponse error ->
            ( { model | error = Just ( "An Error Occured", WS.errorToString error ) }, Cmd.none )

        _ ->
            case WS.reconnectedResponses response of
                [] ->
                    ( model, Cmd.none )

                _ ->
                    ( { model | apiState = Idle [] }, Cmd.none )


handleServerMessage : Model -> Messages.ServerMessage -> Model
handleServerMessage model message =
    case message of
        Messages.ServerGameUpdate game ->
            case model.apiState of
                InGame rec ->
                    { model | apiState = InGame { rec | game = game } }

                Idle _ ->
                    { model | apiState = InGame { game = game, selected = Nothing } }

                _ ->
                    { model | error = Just outOfSyncError }

        Messages.ServerGameListUpdate gameList ->
            case model.apiState of
                Idle _ ->
                    { model | apiState = Idle gameList }

                Connected ->
                    { model | apiState = Idle gameList }

                _ ->
                    { model | error = Just outOfSyncError }

        Messages.ServerError errorMsg ->
            { model | error = Just ( "Server Error", errorMsg ) }

        Messages.ServerProvideID id ->
            { model | playerID = Just id }


sendClientMessage : Model -> Messages.ClientMessage -> Cmd Msg
sendClientMessage model msg =
    wsSend model <|
        WS.makeSend model.backendUrl <|
            Json.Encode.encode 0 <|
                Messages.clientMessageEncoder msg



-- ERRORS


outOfSyncError : ( String, String )
outOfSyncError =
    ( "Api out of sync", "Somehow the server and the client are out of sync" )
