module Popups exposing (Msg(..), Popup(..), gameSettingsPopup, update, view, viewError)

import Api.Game as Game
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Style



-- TYPES


type Popup
    = Name String
    | CreateGame String String
    | LeaveGame String
    | GameSettings Game.Settings


type Msg
    = Close
    | Ok
    | Internal InternalMsg
    | RemoveError


type InternalMsg
    = UpdateName String
    | UpdateGameName String
    | SettingsAutoEndGame Bool
    | SettingsCardRequest Game.CardRequestRule
    | SettingsWrongPick Game.WrongPickRule



-- VIEW


view : Popup -> Element Msg
view popup =
    case popup of
        Name name ->
            selectNamePopup name

        CreateGame name gameName ->
            createGamePopup name gameName

        LeaveGame _ ->
            leaveGamePopup

        GameSettings settings ->
            gameSettingsPopup settings


viewError : ( String, String ) -> Element Msg
viewError ( title, message ) =
    el
        [ padding Style.errorPadding
        , alignTop
        , alignRight
        ]
    <|
        Input.button
            []
            { onPress = Just RemoveError
            , label = Style.warningBox title message
            }



-- UPDATE


update : Popup -> InternalMsg -> Popup
update popup msg =
    case popup of
        Name _ ->
            case msg of
                UpdateName name ->
                    Name name

                _ ->
                    popup

        CreateGame name gameName ->
            case msg of
                UpdateName newName ->
                    CreateGame newName gameName

                UpdateGameName newName ->
                    CreateGame name newName

                _ ->
                    CreateGame name gameName

        LeaveGame url ->
            LeaveGame url

        GameSettings settings ->
            case msg of
                SettingsAutoEndGame b ->
                    GameSettings { settings | autoEndGame = b }

                SettingsCardRequest o ->
                    GameSettings { settings | cardRequestRule = o }

                SettingsWrongPick o ->
                    GameSettings { settings | wrongPickRule = o }

                _ ->
                    GameSettings settings



-- POPUPS


selectNamePopup : String -> Element Msg
selectNamePopup name =
    basicPopup "Select a Nickname" <|
        Input.text []
            { onChange = \s -> Internal <| UpdateName s
            , text = name
            , placeholder =
                Just <| Input.placeholder [ Font.color <| Style.darken 0.5 <| rgb 1 1 1 ] <| text "Nickname"
            , label = Input.labelAbove [] <| text "Nickname"
            }


createGamePopup : String -> String -> Element Msg
createGamePopup name gameName =
    basicPopup "Create a new game" <|
        column [ spacing Style.popupPadding ]
            [ Input.text []
                { onChange = \s -> Internal <| UpdateName s
                , text = name
                , placeholder =
                    Just <| Input.placeholder [ Font.color <| Style.darken 0.5 <| rgb 1 1 1 ] <| text "Nickname"
                , label = Input.labelAbove [] <| text "Player Nickname"
                }
            , Input.text []
                { onChange = \s -> Internal <| UpdateGameName s
                , text = gameName
                , placeholder =
                    Just <| Input.placeholder [ Font.color <| Style.darken 0.5 <| rgb 1 1 1 ] <| text "Game Name"
                , label = Input.labelAbove [] <| text "Game Name"
                }
            ]


leaveGamePopup : Element Msg
leaveGamePopup =
    basicPopup "Are you sure you want to leave this game?" <|
        text "Your points will be lost"


gameSettingsPopup : Game.Settings -> Element Msg
gameSettingsPopup settings =
    basicPopup "Game Settings" <|
        column [ spacing Style.popupPadding ]
            [ Input.checkbox []
                { onChange = \b -> Internal <| SettingsAutoEndGame b
                , icon = Input.defaultCheckbox
                , checked = settings.autoEndGame
                , label = Input.labelAbove [] <| text "Auto end game"
                }
            , Input.radio []
                { onChange = \o -> Internal <| SettingsCardRequest o
                , options =
                    [ Input.option Game.SingleRequest <| text "Single Request"
                    , Input.option (Game.PercentageRequest 0.5) <| text "More than halve"
                    ]
                , selected = Just settings.cardRequestRule
                , label = Input.labelAbove [] <| text "Card request rule"
                }
            , Input.radio []
                { onChange = \o -> Internal <| SettingsWrongPick o
                , options =
                    [ Input.option Game.NoAction <| text "No action"
                    , Input.option Game.NegativePoint <| text "Negative point"
                    , Input.option Game.BlockedForRound <| text "Blocked for one round"
                    ]
                , selected = Just settings.wrongPickRule
                , label = Input.labelAbove [] <| text "Wrong pick rule"
                }
            ]


basicPopup : String -> Element Msg -> Element Msg
basicPopup title content =
    column
        [ centerX
        , centerY
        , Background.color Style.popupColor
        , Border.width Style.popupBorder
        , Border.color <| Style.darken 0.5 Style.popupColor
        , padding Style.popupPadding
        , spacing Style.popupPadding
        ]
        [ row [ width fill, height Style.popupTitleHeight, spacing Style.popupPadding ]
            [ el [ Font.bold ] <| text title
            , el [ alignTop, alignRight ] <| Style.closeButton Close
            ]
        , content
        , row [ width fill, spacing Style.popupPadding ]
            [ el [ alignRight ] <| Style.cancleButton Close
            , el [ alignRight ] <| Style.okButton Ok
            ]
        ]
