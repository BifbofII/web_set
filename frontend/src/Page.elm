module Page exposing (Msg(..), Page(..), view)

import Api.Game as Game
import Dict
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import GameUtils
import Style



-- PAGES


type Page
    = Connecting
    | Lobby (List Game.GameDesc)
    | Game String Game.Game (Maybe ( Int, Maybe Int ))
    | Error String String



-- MESSAGES


type Msg
    = JoinGame String
    | CreateGame
    | StartGame
    | ResetGame
    | RequestCards
    | SelectCard Int
    | OpenSettings



-- VIEW


view : Page -> ( String, Element Msg )
view page =
    ( pageTitle page
    , column
        [ width fill
        , height fill
        ]
        [ el
            [ Region.navigation, width fill ]
          <|
            pageHeader page
        , el
            [ Region.mainContent
            , width fill
            , height fill
            ]
          <|
            el
                [ width (fill |> maximum Style.contentMaxWidth)
                , height fill
                , centerX
                , centerY
                , padding Style.contentPadding
                ]
            <|
                pageContent page
        , el
            [ Region.footer, width fill ]
          <|
            pageFooter page
        ]
    )


pageTitle : Page -> String
pageTitle page =
    case page of
        Connecting ->
            "Web Set - Connecting"

        Lobby _ ->
            "Web Set - Lobby"

        Game _ game _ ->
            case game of
                Game.Initialized rec ->
                    "Web Set - " ++ rec.gameName

                Game.Active rec ->
                    "Web Set - " ++ rec.gameName

                Game.Ended rec ->
                    "Web Set - " ++ rec.gameName

                Game.Orphaned rec ->
                    "Web Set - " ++ rec.gameName

        Error _ _ ->
            "Web Set - Error"


pageContent : Page -> Element Msg
pageContent page =
    case page of
        Connecting ->
            el [ centerX, centerY ] <|
                Style.infoBox "Connecting to server" "Waiting for response from backend..."

        Lobby descList ->
            gameList descList

        Game pid game selected ->
            let
                ( mSelf, others ) =
                    GameUtils.getSelfAndOthers pid game
            in
            case mSelf of
                Nothing ->
                    Style.warningBox "This should not have happened" "Your own ID was not found amongst the players"

                Just self ->
                    row
                        [ width fill
                        , height fill
                        , spacing Style.gameSpacing
                        ]
                        [ el [ width <| fillPortion Style.gameBoardPortion, height fill ] <| gameBoard pid game selected
                        , el [ width <| fillPortion 1, height fill ] <| playerList self others
                        ]

        Error title msg ->
            Style.warningBox title msg


pageHeader : Page -> Element Msg
pageHeader page =
    let
        right =
            case page of
                Game pid game _ ->
                    let
                        ownerID =
                            case game of
                                Game.Initialized gRec ->
                                    gRec.gameOwner.playerID

                                Game.Active gRec ->
                                    gRec.gameOwner.playerID

                                Game.Ended gRec ->
                                    gRec.gameOwner.playerID

                                Game.Orphaned _ ->
                                    "INVALID"
                    in
                    if pid == ownerID then
                        Style.settingsButton OpenSettings

                    else
                        none

                _ ->
                    none
    in
    headerFooter none (el [ Style.headerFontWeight ] <| text <| pageTitle page) right


pageFooter : Page -> Element msg
pageFooter _ =
    headerFooter none
        (link
            Style.linkStyle
            { url = "https://gitlab.com/BifbofII/web_set", label = text "Source Code" }
        )
        none


headerFooter : Element msg -> Element msg -> Element msg -> Element msg
headerFooter left center right =
    row
        [ width fill
        , height <| px Style.headerFooterHeight
        , Background.color Style.headerFooterColor
        , Font.color Style.headerFooterFontColor
        , padding Style.headerFooterPadding
        , spacing Style.headerFooterPadding
        ]
        [ el
            [ width <| fillPortion 1, height fill ]
          <|
            el [ centerX, centerY ] left
        , el
            [ width <| fillPortion Style.headerFooterMiddlePortion, height fill ]
          <|
            el [ centerX, centerY ] center
        , el
            [ width <| fillPortion 1, height fill ]
          <|
            el [ centerX, centerY ] right
        ]



-- COMPONENTS


gameList : List Game.GameDesc -> Element Msg
gameList list =
    column
        [ width (fill |> maximum Style.gameListWidth)
        , height (fill |> maximum Style.gameListHeight)
        , centerX
        , centerY
        , padding Style.gameListPadding
        , spacing Style.gameListPadding
        , Background.color Style.gameListColor
        , Border.color <| Style.darken 0.5 Style.gameListColor
        , Border.width Style.gameListBorder
        , Border.rounded Style.gameListRounding
        ]
        [ row
            [ width fill ]
            [ el
                [ Font.bold
                , Font.size Style.gameListTitleSize
                ]
              <|
                text "Game List"
            , el [ alignTop, alignRight ] <| Style.addButton CreateGame
            ]
        , column
            [ height fill
            , width fill
            , clipY
            , scrollbarY
            , Border.color <| Style.darken 0.5 Style.gameListColor
            , Border.width Style.gameListBorder
            ]
          <|
            List.map gameDesc list
        ]


gameDesc : Game.GameDesc -> Element Msg
gameDesc desc =
    el [ width fill ] <|
        Input.button
            [ width fill
            , height Style.gameDescHeight
            , Background.color Style.gameDescColor
            , Border.color <| Style.darken 0.5 Style.gameDescColor
            , Border.width Style.gameDescBorder
            , focused
                [ Background.color Style.gameDescColorSelected
                , Border.color <| Style.darken 0.5 Style.gameDescColorSelected
                ]
            , mouseOver
                [ Background.color Style.gameDescColorSelected
                , Border.color <| Style.darken 0.5 Style.gameDescColorSelected
                ]
            ]
            { onPress = Just <| JoinGame desc.gameDescID
            , label =
                row
                    [ width fill
                    , height fill
                    , centerX
                    , centerY
                    , padding Style.gameDescPadding
                    , spacing Style.gameDescPadding
                    ]
                    [ el [ Font.bold, width fill ] <| text desc.gameDescName
                    , column [ width fill ]
                        [ el [ Font.bold ] <| text "Game State"
                        , text desc.gameDescState
                        ]
                    , column [ width fill ]
                        [ el [ Font.bold ] <| text "Number of Players"
                        , text <| Debug.toString desc.gameDescNumPlayers
                        ]
                    ]
            }


gameBoard : String -> Game.Game -> Maybe ( Int, Maybe Int ) -> Element Msg
gameBoard pid game selected =
    el
        [ width fill
        , height fill
        , Background.color Style.gameBoardColor
        , Border.width Style.gameBoardBorder
        , Border.color <| Style.darken 0.5 Style.gameBoardColor
        , Border.rounded Style.gameBoardRounding
        , padding Style.gameBoardPadding
        ]
    <|
        case game of
            Game.Initialized rec ->
                el [ centerX, centerY ] <|
                    if pid == rec.gameOwner.playerID then
                        el [ Font.size Style.gameBoardTitleSize ] <|
                            Style.textButton StartGame Style.green "Start Game"

                    else
                        Style.infoBox "Waiting for game to start" "The game owner needs to start the game"

            Game.Active rec ->
                column
                    [ width fill
                    , height fill
                    , spacing Style.gameBoardPadding
                    ]
                    [ board rec.gameBoard selected
                    , row
                        [ width fill
                        , spacing Style.gameBoardPadding
                        ]
                        [ el [ Font.bold ] <| text "Remaining cards:"
                        , text <| Debug.toString rec.gameDeck
                        , el [ alignRight ] <| Style.textButton RequestCards Style.blue "Request additional cards"
                        ]
                    ]

            Game.Ended rec ->
                el [ centerX, centerY ] <|
                    column [ spacing Style.gameBoardPadding ]
                        [ el [ Font.size Style.gameBoardTitleSize ] <| text "Game ended"
                        , if pid == rec.gameWinner then
                            text "Congratulations, you won!"

                          else
                            text <|
                                if rec.gameOwner.playerID == rec.gameWinner then
                                    "The winner is " ++ rec.gameOwner.playerName

                                else
                                    case Dict.get rec.gameWinner rec.gamePlayers of
                                        Nothing ->
                                            "The winner was not found"

                                        Just winner ->
                                            "The winner is " ++ winner.playerName
                        , if pid == rec.gameOwner.playerID then
                            el [ Font.size Style.gameBoardTitleSize ] <|
                                Style.textButton ResetGame Style.green "Restart Game"

                          else
                            none
                        ]

            Game.Orphaned _ ->
                el [ centerX, centerY ] <|
                    Style.warningBox "Game Orphaned" "This state should not be possible"


board : Game.Board -> Maybe ( Int, Maybe Int ) -> Element Msg
board b selected =
    row
        [ width fill
        , height fill
        , spacing Style.gameBoardPadding
        ]
    <|
        List.indexedMap (threeCards selected) <|
            GameUtils.boardToCardList b


threeCards : Maybe ( Int, Maybe Int ) -> Int -> ( Game.Card, Game.Card, Game.Card ) -> Element Msg
threeCards sel baseind ( c1, c2, c3 ) =
    let
        issel =
            \c ->
                case sel of
                    Nothing ->
                        False

                    Just s1 ->
                        case Tuple.second s1 of
                            Nothing ->
                                c == Tuple.first s1

                            Just s2 ->
                                c == Tuple.first s1 || c == s2

        ind =
            baseind * 3
    in
    column
        [ width fill
        , height fill
        , spacing Style.gameBoardPadding
        ]
        [ card (issel ind) ind c1, card (issel <| ind + 1) (ind + 1) c2, card (issel <| ind + 2) (ind + 2) c3 ]


card : Bool -> Int -> Game.Card -> Element Msg
card sel ind c =
    el
        [ width fill
        , height fill
        , centerX
        , centerY
        ]
    <|
        Input.button
            [ width fill
            , height fill
            , Background.color Style.cardColor
            , Border.width Style.cardBorder
            , if sel then
                Border.color Style.cardBorderColorSelected

              else
                Border.color Style.cardBorderColor
            , Border.rounded Style.cardRounding
            , mouseOver
                [ if sel then
                    Border.color Style.cardBorderColorSelectedHover

                  else
                    Border.color Style.cardBorderColorHover
                ]
            ]
            { onPress = Just <| SelectCard ind
            , label = cardSymbols c
            }


cardSymbols : Game.Card -> Element Msg
cardSymbols c =
    column
        [ centerX
        , centerY
        , padding Style.cardPadding
        , spacing Style.cardSpacing
        , width shrink
        , height shrink
        ]
    <|
        case c.number of
            Game.One ->
                [ shape c.shape c.fill c.color ]

            Game.Two ->
                [ shape c.shape c.fill c.color
                , shape c.shape c.fill c.color
                ]

            Game.Three ->
                [ shape c.shape c.fill c.color
                , shape c.shape c.fill c.color
                , shape c.shape c.fill c.color
                ]


shape : Game.Shape -> Game.Fill -> Game.Color -> Element Msg
shape s f c =
    let
        shapeStr =
            case s of
                Game.Oval ->
                    "ov-"

                Game.Diamond ->
                    "dia-"

                Game.Wave ->
                    "wav-"

        fillStr =
            case f of
                Game.Empty ->
                    "em.svg"

                Game.Half ->
                    "ha.svg"

                Game.Full ->
                    "fu.svg"

        colorStr =
            case c of
                Game.Red ->
                    "rd-"

                Game.Green ->
                    "gr-"

                Game.Violet ->
                    "vi-"
    in
    image
        [ width fill, height (fill |> maximum Style.cardMaxSymbHeight) ]
        { src = "icons/symbols/" ++ shapeStr ++ colorStr ++ fillStr
        , description = ""
        }


playerList : Game.Player -> List Game.Player -> Element Msg
playerList self others =
    column
        [ width fill
        , height fill
        , padding Style.playerListPadding
        , spacing Style.playerListPadding
        , Background.color Style.playerListColor
        , Border.color <| Style.darken 0.5 Style.playerListColor
        , Border.width Style.playerListBorder
        , Border.rounded Style.playerListRounding
        ]
        [ el
            [ Font.bold
            , Font.size Style.playerListTitleSize
            ]
          <|
            text "Players"
        , playerDisplay Style.playerColorSelf self
        , column [ width fill, height fill, clipX ] <|
            List.map (playerDisplay Style.playerColor) others
        ]


playerDisplay : Color -> Game.Player -> Element Msg
playerDisplay color player =
    column
        [ width fill
        , height Style.playerHeight
        , padding Style.playerPadding
        , spacing Style.playerPadding
        , Background.color color
        , Border.color <| Style.darken 0.5 color
        , Border.width Style.playerBorder
        ]
        [ el [ Font.bold, width fill, centerX, centerY ] <| text player.playerName
        , el [ centerX, centerY ] <| text <| Debug.toString player.playerPoints
        ]
