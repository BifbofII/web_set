module GameUtils exposing (boardToCardList, getPlayers, getSelfAndOthers)

import Api.Game as Game
import Dict


getSelfAndOthers : String -> Game.Game -> ( Maybe Game.Player, List Game.Player )
getSelfAndOthers id game =
    let
        players =
            getPlayers game
    in
    ( Dict.get id players, Dict.values <| Dict.remove id players )


getPlayers : Game.Game -> Dict.Dict String Game.Player
getPlayers game =
    case game of
        Game.Orphaned {} ->
            Dict.empty

        Game.Initialized rec ->
            Dict.insert rec.gameOwner.playerID rec.gameOwner rec.gamePlayers

        Game.Active rec ->
            Dict.insert rec.gameOwner.playerID rec.gameOwner rec.gamePlayers

        Game.Ended rec ->
            Dict.insert rec.gameOwner.playerID rec.gameOwner rec.gamePlayers


boardToCardList : Game.Board -> List ( Game.Card, Game.Card, Game.Card )
boardToCardList board =
    case board of
        Game.Normal c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 ->
            [ (c1, c2, c3), (c4, c5, c6), (c7, c8, c9), (c10, c11, c12) ]

        Game.Added1 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 c13 c14 c15 ->
            [ (c1, c2, c3), (c4, c5, c6), (c7, c8, c9), (c10, c11, c12), (c13, c14, c15) ]

        Game.Added2 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 c13 c14 c15 c16 c17 c18 ->
            [ (c1, c2, c3), (c4, c5, c6), (c7, c8, c9), (c10, c11, c12), (c13, c14, c15), (c16, c17, c18) ]

        Game.Added3 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 c13 c14 c15 c16 c17 c18 c19 c20 c21 ->
            [ (c1, c2, c3), (c4, c5, c6), (c7, c8, c9), (c10, c11, c12), (c13, c14, c15), (c16, c17, c18), (c19, c20, c21) ]

        Game.Reduced1 c1 c2 c3 c4 c5 c6 c7 c8 c9 ->
            [ (c1, c2, c3), (c4, c5, c6), (c7, c8, c9) ]

        Game.Reduced2 c1 c2 c3 c4 c5 c6 ->
            [ (c1, c2, c3), (c4, c5, c6) ]

        Game.Reduced3 c1 c2 c3 ->
            [ (c1, c2, c3) ]

        Game.InvalidBoard ->
            []
